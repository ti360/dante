<?php
session_start();
?>
<?php //require_once("includes/conexion.php"); 
  require("includes/constantes.php");
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico"> -->

    <title>Login Dante</title>

    <!-- Bootstrap core CSS --> 
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <!-- Custom styles for this template -->
    <link href="css/signin.css" rel="stylesheet">
  
  </head>
  <body>
    <!-- Gestion de acceso con la bbdd -->
    <?php
      if(isset($_SESSION["session_username"])){     
       header("Location: PlantillaDanteBase.html");
      }
      if(isset($_POST["login"])){
        if(!empty($_POST['inputUser']) && !empty($_POST['inputPassword']) && !empty($_POST['inputCentro'])) {
          $usuarioCliente=$_POST['inputUser'];
          $claveCliente=$_POST['inputPassword'];
          $centro=$_POST['inputCentro'];
          /* Creamos la conexión dinámicamente */
          $servidores = simplexml_load_file("dante.xml");
          $arrayservidores=array();
          foreach($servidores as $servidor){
            if( $servidor->nombre == $centro){
              $ip=$servidor->ip;
              $bbdd=$servidor->bbdd;
            }
          }
          /* Consulta para comprobar credenciales */ 
            $con = @mysqli_connect($ip, DB_USER, DB_PASS, $bbdd);
            if ($con){
              //mysqli_select_db($con, $bbdd) or die("La base de datos no puede ser seleccionada!");
              $consulta =mysqli_query($con, "SELECT * FROM usuarios WHERE nombre='".$usuarioCliente."' AND password='".$claveCliente."'");
              $numeroFilas=mysqli_num_rows($consulta);
              if($numeroFilas!=0){
                $_SESSION['session_username']=$usuarioCliente;
                /* Redireccionar el navegador */
                header("Location: PlantillaDanteBase.html");
              } 
              else {$mensaje =  "Nombre de usuario o contraseña invalida!";}
            }
            else {$mensaje = "No se puede establecer una conexión con el equipo de destino!";}         
        } 
        else {$mensaje = "Todos los campos son requeridos!";}
      }
    ?>
    <!-- Formulario de petición de datos -->
    <div class="container-fluid">
        <div class="row filaformulario align-items-center justify-content-center mt-4">
          <form class="form-signin" name="loginform" id="loginform"  action="" method="POST" autocomplete="off">
            <img class="mb-4" src="images/ti360.png" alt="" width="100" height="72">
            <h1 class="h3 mb-3 font-weight-normal" style="color:darkcyan">Identificación</h1>    
            <!-- Usuario -->
            <label for="inputUser" class="sr-only">Usuario</label>
            <input type="text" name="inputUser" id="inputUser" class="form-control" placeholder="Usuario" required autofocus>
            <!-- Password -->
            <label for="inputPassword" class="sr-only">Password</label>
            <input type="password" name="inputPassword" id="inputPassword" class="form-control" placeholder="Password" required>
            <!-- Servidor -->
            <select name="inputCentro" id="inputCentro" class="form-control"> 
            <?php
               $servidores = simplexml_load_file("dante.xml");
               $arrayservidores=array();
               foreach($servidores as $servidor){
                  echo "<option value=".$servidor->nombre.">".$servidor->nombre."</option>";
                  array_push($arrayservidores, array($servidor->nombre,$servidor->ip,$servidor->bbdd));
                }
            ?>
            </select>
            <!-- Recuerdame -->
            <div class="checkbox mb-3">
              <label style="color: darkcyan;">
                <input type="checkbox" value="remember-me"> Recuerdame
              </label>
            </div>
            <!-- Boton accede -->
            <p class="submit">
              <input class="btn btn-lg btn-primary btn-block"  type="submit" name="login" value="Accede"/>
            </p>
            <p class="mt-5 mb-3 text-white">&copy; 2019</p>
          </form>
        </div>
        <!-- Mensaje en caso de error -->
        <?php    
            if (!empty($mensaje)) {echo '<p class="errorm" align="center">' .$mensaje . "</p>";} 
        ?>
    </div>
  </body>
</html>